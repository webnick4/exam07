import React, { Component } from 'react';
import './App.css';
import AddItems from "../components/AddItems/AddItems";
import OrderDetails from "../components/OrderDetails/OrderDetails";

class App extends Component {
  state = {
    menu: [
      {type: 'Hamburger', amount: 0, price: 80, sum: 0, icon: 'fa fa-cutlery'},
      {type: 'Cheeseburger', amount: 0, price: 90, sum: 0, icon: 'fa fa-cutlery'},
      {type: 'Fries', amount: 0, price: 45, sum: 0, icon: 'fa fa-cutlery'},
      {type: 'Coffee', amount: 0, price: 70, sum: 0, icon: 'fa fa-coffee'},
      {type: 'Tea', amount: 0, price: 50, sum: 0, icon: 'fa fa-coffee'},
      {type: 'Cola', amount: 0, price: 40, sum: 0, icon: 'fa fa-coffee'}
    ],
    totalPrice: 0
  };

  increaseAmount = (index) => {
    const menu = [...this.state.menu];
    const typeIndex = {...menu[index]};
    typeIndex.amount++;
    typeIndex.sum = typeIndex.sum + typeIndex.price;

    menu[index] = typeIndex;
    let totalPrice = this.state.totalPrice;
    totalPrice += this.state.menu[index].price;

    this.setState({menu, totalPrice});
  };

  removeProduct = (index) => {
    const menu = [...this.state.menu];
    const typeIndex = {...menu[index]};
    typeIndex.amount--;
    typeIndex.sum = typeIndex.sum - typeIndex.price;

    menu[index] = typeIndex;
    let totalPrice = this.state.totalPrice;
    totalPrice -= this.state.menu[index].price;

    this.setState({menu, totalPrice});
  };

  render() {
    let menu = null;
    menu = <AddItems
      menu={this.state.menu}
      click={this.increaseAmount}
    />;

    let details = null;
    details = <OrderDetails
      details={this.state.menu}
      total={this.state.totalPrice}
      remove={this.removeProduct}
    />;


    return (
      <div className="App">
        <h2>Welcome! Make your order.</h2>
        {details}
        {menu}
      </div>
    );
  }
}

export default App;
