import React from 'react';
import './Item.css';

const Item = (props) => {
  return (
    <div className="Item" onClick={props.click}>
      <i className={props.icon} aria-hidden="true"></i>
      <h3>{props.type}</h3>
      <p>Price: {props.price} KGS</p>
    </div>
  );
};

export default Item;