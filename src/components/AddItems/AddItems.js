import React from 'react';
import './AddItems.css';
import Item from "./Item/Item";


const AddItems = (props) => {
  return (
    <div className="AddItems">
      {
        props.menu.map((item, index) => {
          return <Item
            key={index}
            icon={item.icon}
            type={item.type}
            price={item.price}
            click={() => props.click(index)}
            />
        })
      }
    </div>
  );
};

export default AddItems;