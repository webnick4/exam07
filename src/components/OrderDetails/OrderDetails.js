import React from 'react';
import './OrderDetails.css';
import OrderItem from "./OrderItem/OrderItem";


const OrderDetails = (props) => {
  if (props.total !== 0) {
    return (
      <div className="OrderDetails">
        {
          props.details.map((item, index) => {
            if (item.amount === 0) {
              return null;
            } else {
              return <OrderItem
                key={index}
                type={item.type}
                amount={item.amount}
                price={item.sum}
                remove={() => props.remove(index)}
              />
            }
          })
        }

        <p>Total price: {props.total}</p>
      </div>
    );
  } else {
    return (
      <div className="OrderDetails">
        <p>Order is empty!</p>
        <p>Please add some items!</p>
      </div>
    )
  }

};

export default OrderDetails;