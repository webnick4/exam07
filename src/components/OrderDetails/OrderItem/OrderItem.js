import React from 'react';
import './OrderItem.css';

const OrderItem = (props) => {
  return (
    <div>
      <p>{props.type} x{props.amount} {props.sum} <button className="OrderItem-remove" onClick={props.remove}> remove </button></p>
    </div>

  );
};

export default OrderItem;